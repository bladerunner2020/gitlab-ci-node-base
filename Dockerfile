FROM node:14.18.1-alpine3.11
RUN apk -q update && apk add --no-cache -q \
    git \
    bash \
    curl \
    openssh-client \
    && rm -rf /var/cache/apk/*

RUN mkdir -p /root/.ssh
RUN touch /root/.ssh/known_hosts
RUN ssh-keyscan gitlab.com >> /root/.ssh/known_hosts
RUN ssh-keyscan github.com >> /root/.ssh/known_hosts
RUN ssh-keyscan git.booco.ru >> /root/.ssh/known_hosts

