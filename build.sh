#!/bin/bash
docker build -t bladerunner2020/gitlab-ci-node-base --platform linux/amd64 .
docker image push bladerunner2020/gitlab-ci-node-base

if [ "$1" ]; then
  docker tag bladerunner2020/gitlab-ci-node-base:latest bladerunner2020/gitlab-ci-node-base:$1
  docker image push bladerunner2020/gitlab-ci-node-base:$1
fi
